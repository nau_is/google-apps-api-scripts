#!/usr/bin/python


###############
# This script queries the Google Admin API and collects a list of
# logins from approximately the last half-hour and then dumps them 
# into a file for use with Splunk. 
#
#	JSON Results -> output.log
#	oAuth Creds -> working_dir/crds.dat (Don't touch this)
#	
#	This script exits with a status of 0 if it runs completely,
#	anything else means it broke. 
###############

#For making HTTP Requests
import httplib2
from urllib import urlencode

#Google API Stuff, creates the service object that you get stuff from
from apiclient import errors
from apiclient.discovery import build

#DateTime
from datetime import datetime, timedelta

#For parsing command-line arguments
import sys
import argparse

#OAuth2 Stuff
from oauth2client import file
from oauth2client import client
from oauth2client import tools
import json

def main():

	#Output file for storing results
	output = open("output.log", "a")

	#Create the credentials object and fill it from storage.
	storage = file.Storage('working_dir/crds.dat')
	credentials = storage.get()

	if credentials is None or credentials.access_token_expired:
		print "Your credentials are invalid, I'm refreshing them!"
		credentials.refresh(http=httplib2.Http())
		storage.put(credentials)

	# Create an httplib2.Http object and authorize it with our credentials
	http = httplib2.Http()
	http = credentials.authorize(http)

	# An object representing 30 minutes ago MST
	thirty_minutes_ago = datetime.utcnow() - timedelta(minutes=30)
	start_time = thirty_minutes_ago.isoformat('T') + 'Z'

	# Build the service with apiclient.discovery.build()
	reports_service = build('admin', 'reports_v1', http=http)

	all_logins = []
	page_token = None
	params = {'applicationName': 'login', 'userKey': 'all', 'startTime': start_time}
	#populate the output file with responses from google
	i=1
	while True:
		try:
			print "page %i" % i
			i+=1
			if i == 50:
				print "Man, that's a lot of pages..."
			#check if google supplied the next page
			if page_token:
				params['pageToken'] = page_token
			#Request a list of logins from google
			current_page = reports_service.activities().list(**params).execute()
			for items in current_page["items"]:
				output.write(json.dumps(items, indent=2))
				output.write("\n")
			#Grab the token for the next page and update it for the next loop
			page_token = current_page.get('nextPageToken')
			if not page_token:
				break
		except errors.HttpError as error:
			print 'An error occurred: %s' % error
			break
		except KeyError as error: 
			break 
	# if all_logins:		
	# 	for activity in all_logins:
	# 		output.write(json.dumps(activity, indent=2))
	# 		output.write("\n")
	sys.exit(0)

if __name__ == "__main__":
	main()
