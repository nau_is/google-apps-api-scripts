#!/usr/bin/python

#############
#	This script authorizes applications to access Google APIs. In order 
#	for it to work, a file structure must be observed:
#
#		/App_Folder
#			|app_name.py
#			|output_file.txt	(optional)
#			|/working_dir 			(Strict naming conventions from here on out)
#				|crds.dat 			(for storing an oAuth2 credentials object)
#				|client_secret.json (downloaded from the Google Admin Dashboard, used to create oAuth2 Flow)
#				|scope.json			(used to store the application scope for the script being authorized, see scope-example.json in this directory)
#############
#For making HTTP Requests
import httplib2
#OAuth2 Stuff
from oauth2client import file
from oauth2client import client
from oauth2client import tools
#for paths
import os
#for sys.exit()
import sys
#for json
import json

def get_creds(key_store, crds, app_scope, client_secret):
	#############
	#	This function steps through the oAuth2 flow. It is the meat of this program. 
	#	This function is kind of complicated when it comes to the parameters, and for that I'm sorry. 
	#	It expects the following:
	#		key_store 	-> 	type: oauth2client.file.storage()
	#		crds 		-> 	type: String from oauth2client.file.storage().get()
	#		app_scope	-> 	type: String from json.loads()	
	#		client_secret ->type: os.path
	#############

	# Flow from working_dir/client_secret.json
	flow = client.flow_from_clientsecrets(client_secret, scope=app_scope["Scope"], redirect_uri='urn:ietf:wg:oauth:2.0:oob');

	# This steps through an OAuth2 authentication flow
	# Requires a pointer to a oauth2client.file.storage object
	# 
	# For hints, see: https://developers.google.com/api-client-library/python/guide/aaa_oauth#flows
	if crds is None or crds.invalid:
		authorize_url = flow.step1_get_authorize_url()
		print 'Go to the following link in your browser: ' + authorize_url
		code = raw_input('Enter verification code: ').strip()
		credentials = flow.step2_exchange(code)
		key_store.put(credentials)
		return credentials
	else: 
		return crds

def main():
	print "Welcome to the NAU Google Script Authorization Utility."
	while True:
		#grab the path/to/the/program from the user
		path = raw_input("Enter the absolute path of the app to be authorized: ")
		try:
			prog_path = os.path.realpath(path)
			break
		except:
			print "I'm sorry, that is not a valid path, try again."
	#get the parent directory and other files so we can do werk
	parent_dir = os.path.abspath(os.path.join(prog_path, os.pardir))
	client_secret_path = os.path.join(parent_dir, "working_dir/client_secret.json")
	app_scope_path = os.path.join(parent_dir, "working_dir/scope.json")
	cred_store_path = os.path.join(parent_dir, "working_dir/crds.dat")
	refresh_store_path = os.path.join(parent_dir, "working_dir/refresh.dat")

	#check to see if everything is where it should be
	if not os.path.isfile(client_secret_path):
		print client_secret_path
		print "Can't find your client secret. Dying..."
		sys.exit(1)
	if not os.path.isfile(app_scope_path):
		print "Can't find your application scope file. Dying..."
		sys.exit(1)
	if not os.path.isfile(cred_store_path):
		print "Couldn't find your credential store, so I made one for you!"
		store = open(cred_store_path, "w")
		store.close()
	if not os.path.isfile(refresh_store_path):
		print "Couldn't find your refresh token store, so I made one for you!"
		store = open(refresh_store_path, "w")
		store.close()

	#open up the cred_store for reading/writing
	storage = file.Storage(cred_store_path)
	credentials = storage.get()
	#open up the app_scope and parse it for json
	scope = open(app_scope_path).read()
	scope = json.loads(scope)
	#get the credentials
	credentials = get_creds(storage, credentials, scope, client_secret_path)
	#store the creds for the app to use
	storage.put(credentials)

	######
	#	The application is now authorized and has a refresh token, allowing it access to
	#	Google APIs until the token is explicitly revoked. 
	#####
	print "Your app is authorized, go crazy!"
	
	sys.exit(0)

if __name__ == "__main__":
	main()