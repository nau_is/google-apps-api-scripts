# Google Apps Scripts

##What?
These scripts are used to query Google's APIs for information stored in a Google Apps for Education account. These have only been tested with GAforE accounts, however, there's no reason why it wouldn't work with a normal Google user account (for scripts that query APIs that generic accounts have access to, ex. GMail).

##How? 
Each script is a standalone program and is capable of running without intervention from the other scripts in this repo (with the exception of authorizing the individual scripts on first run). 

1. Place script directory where you'd like it in your filesystem. 
2. Run authorize.py and give it the absolute path to the app.py file within the directory you placed in step 1.
3. If there are no errors, then your API Script is now authorized, you may then run the script or set it to run via cron or similar. 

###Authorizing The Scripts

authorize.py authorizes scripts to access Google APIs. Simply run authorize.py on the same machine where the script-to-auth is located. First you'll see this prompt:
	
```
Welcome to the NAU Google Script Authorization Utility.
Enter the absolute path of the app to be authorized: 
```
Just enter the absolute path of the script you'd like to authorize, for example on a mac you might enter: 
`"/Users/[USER]/Documents/Work_Projects/Google\ API\ Scripts/query_google/query.py"`

###Program Structure
If you're going to write your own Google API Script, in order for the authorization process to complete successfully a program file structure must be observed:
```
/App_Folder
	|app_name.py
	|output_file.log 		(optional)
	|/working_dir 			(Strict naming conventions from here on out)
		|crds.dat 			(for storing an oAuth2 credentials object)
		|client_secret.json (downloaded from the Google Admin Dashboard, used to create oAuth2 Flow)
		|scope.json			(used to store the application scope for the script being authorized, see scope-example.json in this directory)
```

###Running the Scripts
Once the script is authorized initially, it will then be able to refresh its oAuth2 token indefinitely, so there's no need to run authorize.py again (probably. I haven't found an instance where I've needed to run it twice). 


Most code written by [Conner Swann](http://connerswann.me)